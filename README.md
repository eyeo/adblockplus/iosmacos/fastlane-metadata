This repository is used by fastlane, an automation tool which allows us to create builds and upload to TestFlight, App Store and other remote locations.

When the latest app version is ready to be uploaded to App Store and released, information (both metadata & screenshots) will be fetched from this repository.